var events = [
["Sugar", "20230412", "13:00", "HUN-2D"],
["Sugar", "20230412", "14:30", "HUN-2D"],
["Sugar", "20230412", "18:00", "HUN-2D"],
["Sugar", "20230412", "20:00", "HUN-2D"],
["Sugar", "20230413", "13:00", "HUN-2D"],
["Sugar", "20230413", "14:30", "HUN-2D"],
["Sugar", "20230413", "18:00", "HUN-2D"],
["Sugar", "20230413", "20:00", "HUN-2D"],
["Sugar", "20230414", "13:00", "HUN-2D"],
["Sugar", "20230414", "14:30", "HUN-2D"],
["Sugar", "20230414", "18:00", "HUN-2D"],
["Sugar", "20230414", "20:00", "HUN-2D"],
["Sugar", "20230415", "13:00", "HUN-2D"],
["Sugar", "20230415", "14:30", "HUN-2D"],
["Sugar", "20230415", "18:00", "HUN-2D"],
["Sugar", "20230415", "20:00", "HUN-2D"],
["Sugar", "20230416", "13:00", "HUN-2D"],
["Sugar", "20230416", "14:30", "HUN-2D"],
["Sugar", "20230416", "18:00", "HUN-2D"],
["Sugar", "20230416", "20:00", "HUN-2D"],
["Sugar", "20230417", "13:00", "HUN-2D"],
["Sugar", "20230417", "14:30", "HUN-2D"],
["Sugar", "20230417", "18:00", "HUN-2D"],
["Sugar", "20230417", "20:00", "HUN-2D"],
["Sugar", "20230418", "13:00", "HUN-2D"],
["Sugar", "20230418", "14:30", "HUN-2D"],
["Sugar", "20230418", "18:00", "HUN-2D"],
["Sugar", "20230418", "20:00", "HUN-2D"],
["Sugar", "20230419", "13:00", "HUN-2D"],
["Sugar", "20230419", "14:30", "HUN-2D"],
["Sugar", "20230419", "18:00", "HUN-2D"],
["Sugar", "20230419", "20:00", "HUN-2D"],
["Corvin", "20230412", "18:15", "HUN-2D"],
["Corvin", "20230412", "20:30", "HUN-2D"],
["Corvin", "20230413", "20:30", "HUN-2D"],
["Corvin", "20230414", "20:30", "HUN-2D"],
["Corvin", "20230415", "20:30", "HUN-2D"],
["Corvin", "20230419", "20:30", "HUN-2D"],
["Allee", "20230412", "12:00", "HUN-2D"],
["Aréna", "20230412", "12:10", "HUN-2D"],
["Campona", "20230412", "13:00", "HUN-2D"],
["Westend", "20230412", "13:20", "HUN-2D"],
["Allee", "20230412", "13:20", "HUN-2D"],
["Aréna", "20230412", "13:30", "HUN-2D"],
["Westend", "20230412", "15:10", "HUN-2D"],
["Allee", "20230412", "15:20", "HUN-2D"],
["Aréna", "20230412", "15:30", "HUN-2D"],
["Mammut I-II.", "20230412", "16:30", "HUN-2D"],
["Duna Pláza", "20230412", "16:40", "HUN-2D"],
["Campona", "20230412", "16:40", "HUN-2D"],
["Westend", "20230412", "16:40", "HUN-2D"],
["Allee", "20230412", "16:40", "HUN-2D"],
["Aréna", "20230412", "16:50", "HUN-2D"],
["Campona", "20230412", "18:30", "HUN-2D"],
["Westend", "20230412", "18:30", "HUN-2D"],
["Allee", "20230412", "18:40", "HUN-2D"],
["Aréna", "20230412", "18:50", "HUN-2D"],
["Aréna", "20230412", "19:00", "HUN-2D"],
["Mammut I-II.", "20230412", "19:45", "HUN-2D"],
["Mammut I-II.", "20230412", "19:45", "HUN-2D"],
["Duna Pláza", "20230412", "20:00", "HUN-2D"],
["Campona", "20230412", "20:00", "HUN-2D"],
["Westend", "20230412", "20:00", "HUN-2D"],
["Allee", "20230412", "20:00", "HUN-2D"],
["Aréna", "20230412", "20:15", "HUN-2D"],
["Westend", "20230412", "21:00", "HUN-2D"],
["Duna Pláza", "20230412", "21:15", "HUN-2D"],
["Mammut I-II.", "20230412", "21:30", "HUN-2D"],
["Aréna", "20230412", "21:30", "HUN-2D"],
["Westend", "20230412", "21:40", "HUN-4DX"],
["Allee", "20230412", "21:45", "HUN-2D"],
["Aréna", "20230412", "21:45", "HUN-2D"],
["Westend", "20230413", "13:15", "HUN-2D"],
["Aréna", "20230413", "13:20", "HUN-2D"],
["Allee", "20230413", "14:00", "HUN-2D"],
["Westend", "20230413", "15:00", "HUN-2D"],
["Aréna", "20230413", "15:15", "HUN-2D"],
["Campona", "20230413", "15:30", "HUN-2D"],
["Aréna", "20230413", "16:40", "HUN-2D"],
["Westend", "20230413", "16:45", "HUN-2D"],
["Campona", "20230413", "17:15", "HUN-2D"],
["Allee", "20230413", "17:30", "HUN-2D"],
["Westend", "20230413", "18:15", "HUN-2D"],
["Aréna", "20230413", "18:40", "HUN-2D"],
["Campona", "20230413", "18:45", "HUN-2D"],
["Mammut I-II.", "20230413", "18:45", "HUN-2D"],
["Campona", "20230413", "19:30", "HUN-2D"],
["Mammut I-II.", "20230413", "20:00", "HUN-2D"],
["Aréna", "20230413", "20:00", "HUN-2D"],
["Duna Pláza", "20230413", "20:15", "HUN-2D"],
["Westend", "20230413", "20:15", "HUN-2D"],
["Allee", "20230413", "20:45", "HUN-2D"],
["Westend", "20230413", "21:30", "HUN-4DX"],
["Aréna", "20230413", "21:40", "HUN-2D"],
["Westend", "20230414", "13:15", "HUN-2D"],
["Aréna", "20230414", "13:20", "HUN-2D"],
["Allee", "20230414", "14:00", "HUN-2D"],
["Westend", "20230414", "15:00", "HUN-2D"],
["Aréna", "20230414", "15:15", "HUN-2D"],
["Campona", "20230414", "15:30", "HUN-2D"],
["Aréna", "20230414", "16:40", "HUN-2D"],
["Westend", "20230414", "16:45", "HUN-2D"],
["Campona", "20230414", "17:15", "HUN-2D"],
["Allee", "20230414", "17:30", "HUN-2D"],
["Westend", "20230414", "18:15", "HUN-2D"],
["Aréna", "20230414", "18:40", "HUN-2D"],
["Campona", "20230414", "18:45", "HUN-2D"],
["Mammut I-II.", "20230414", "18:45", "HUN-2D"],
["Allee", "20230414", "19:00", "HUN-2D"],
["Aréna", "20230414", "19:00", "HUN-2D"],
["Campona", "20230414", "19:45", "HUN-2D"],
["Mammut I-II.", "20230414", "20:00", "HUN-2D"],
["Aréna", "20230414", "20:00", "HUN-2D"],
["Duna Pláza", "20230414", "20:15", "HUN-2D"],
["Westend", "20230414", "20:15", "HUN-2D"],
["Allee", "20230414", "20:45", "HUN-2D"],
["Westend", "20230414", "21:30", "HUN-4DX"],
["Aréna", "20230414", "21:40", "HUN-2D"],
["Campona", "20230415", "12:15", "HUN-2D"],
["Westend", "20230415", "13:15", "HUN-2D"],
["Aréna", "20230415", "13:20", "HUN-2D"],
["Allee", "20230415", "14:00", "HUN-2D"],
["Mammut I-II.", "20230415", "14:30", "HUN-2D"],
["Westend", "20230415", "15:00", "HUN-2D"],
["Mammut I-II.", "20230415", "15:10", "HUN-2D"],
["Aréna", "20230415", "15:15", "HUN-2D"],
["Campona", "20230415", "15:30", "HUN-2D"],
["Aréna", "20230415", "15:30", "HUN-2D"],
["Aréna", "20230415", "16:40", "HUN-2D"],
["Westend", "20230415", "16:45", "HUN-2D"],
["Campona", "20230415", "17:15", "HUN-2D"],
["Allee", "20230415", "17:30", "HUN-2D"],
["Westend", "20230415", "18:15", "HUN-2D"],
["Aréna", "20230415", "18:40", "HUN-2D"],
["Campona", "20230415", "18:45", "HUN-2D"],
["Mammut I-II.", "20230415", "18:45", "HUN-2D"],
["Allee", "20230415", "19:00", "HUN-2D"],
["Aréna", "20230415", "19:00", "HUN-2D"],
["Campona", "20230415", "19:45", "HUN-2D"],
["Mammut I-II.", "20230415", "19:45", "HUN-2D"],
["Aréna", "20230415", "20:00", "HUN-2D"],
["Duna Pláza", "20230415", "20:15", "HUN-2D"],
["Westend", "20230415", "20:15", "HUN-2D"],
["Allee", "20230415", "20:45", "HUN-2D"],
["Westend", "20230415", "21:30", "HUN-4DX"],
["Aréna", "20230415", "21:40", "HUN-2D"],
["Campona", "20230416", "12:15", "HUN-2D"],
["Westend", "20230416", "13:15", "HUN-2D"],
["Aréna", "20230416", "13:20", "HUN-2D"],
["Allee", "20230416", "14:00", "HUN-2D"],
["Mammut I-II.", "20230416", "14:30", "HUN-2D"],
["Westend", "20230416", "15:00", "HUN-2D"],
["Mammut I-II.", "20230416", "15:10", "HUN-2D"],
["Aréna", "20230416", "15:15", "HUN-2D"],
["Campona", "20230416", "15:30", "HUN-2D"],
["Aréna", "20230416", "15:30", "HUN-2D"],
["Aréna", "20230416", "16:40", "HUN-2D"],
["Westend", "20230416", "16:45", "HUN-2D"],
["Campona", "20230416", "17:15", "HUN-2D"],
["Allee", "20230416", "17:30", "HUN-2D"],
["Westend", "20230416", "18:15", "HUN-2D"],
["Aréna", "20230416", "18:40", "HUN-2D"],
["Campona", "20230416", "18:45", "HUN-2D"],
["Mammut I-II.", "20230416", "18:45", "HUN-2D"],
["Allee", "20230416", "19:00", "HUN-2D"],
["Aréna", "20230416", "19:00", "HUN-2D"],
["Campona", "20230416", "19:45", "HUN-2D"],
["Mammut I-II.", "20230416", "19:45", "HUN-2D"],
["Aréna", "20230416", "20:00", "HUN-2D"],
["Duna Pláza", "20230416", "20:15", "HUN-2D"],
["Westend", "20230416", "20:15", "HUN-2D"],
["Allee", "20230416", "20:45", "HUN-2D"],
["Westend", "20230416", "21:30", "HUN-4DX"],
["Aréna", "20230416", "21:40", "HUN-2D"],
["Westend", "20230417", "13:15", "HUN-2D"],
["Aréna", "20230417", "13:20", "HUN-2D"],
["Allee", "20230417", "14:00", "HUN-2D"],
["Westend", "20230417", "15:00", "HUN-2D"],
["Aréna", "20230417", "15:15", "HUN-2D"],
["Campona", "20230417", "15:30", "HUN-2D"],
["Aréna", "20230417", "16:40", "HUN-2D"],
["Westend", "20230417", "16:45", "HUN-2D"],
["Campona", "20230417", "17:15", "HUN-2D"],
["Allee", "20230417", "17:30", "HUN-2D"],
["Westend", "20230417", "18:15", "HUN-2D"],
["Aréna", "20230417", "18:40", "HUN-2D"],
["Campona", "20230417", "18:45", "HUN-2D"],
["Mammut I-II.", "20230417", "18:45", "HUN-2D"],
["Allee", "20230417", "19:00", "HUN-2D"],
["Aréna", "20230417", "19:00", "HUN-2D"],
["Campona", "20230417", "19:45", "HUN-2D"],
["Mammut I-II.", "20230417", "20:00", "HUN-2D"],
["Aréna", "20230417", "20:00", "HUN-2D"],
["Duna Pláza", "20230417", "20:15", "HUN-2D"],
["Westend", "20230417", "20:15", "HUN-2D"],
["Allee", "20230417", "20:45", "HUN-2D"],
["Westend", "20230417", "21:30", "HUN-4DX"],
["Aréna", "20230417", "21:40", "HUN-2D"],
["Westend", "20230418", "13:15", "HUN-2D"],
["Aréna", "20230418", "13:20", "HUN-2D"],
["Allee", "20230418", "14:00", "HUN-2D"],
["Westend", "20230418", "15:00", "HUN-2D"],
["Aréna", "20230418", "15:15", "HUN-2D"],
["Campona", "20230418", "15:30", "HUN-2D"],
["Aréna", "20230418", "16:40", "HUN-2D"],
["Westend", "20230418", "16:45", "HUN-2D"],
["Campona", "20230418", "17:15", "HUN-2D"],
["Allee", "20230418", "17:30", "HUN-2D"],
["Westend", "20230418", "18:15", "HUN-2D"],
["Aréna", "20230418", "18:40", "HUN-2D"],
["Campona", "20230418", "18:45", "HUN-2D"],
["Mammut I-II.", "20230418", "18:45", "HUN-2D"],
["Aréna", "20230418", "19:00", "HUN-2D"],
["Campona", "20230418", "19:45", "HUN-2D"],
["Mammut I-II.", "20230418", "20:00", "HUN-2D"],
["Aréna", "20230418", "20:00", "HUN-2D"],
["Duna Pláza", "20230418", "20:15", "HUN-2D"],
["Westend", "20230418", "20:15", "HUN-2D"],
["Allee", "20230418", "20:45", "HUN-2D"],
["Westend", "20230418", "21:30", "HUN-4DX"],
["Aréna", "20230418", "21:40", "HUN-2D"],
["Westend", "20230419", "13:15", "HUN-2D"],
["Aréna", "20230419", "13:20", "HUN-2D"],
["Allee", "20230419", "14:00", "HUN-2D"],
["Westend", "20230419", "15:00", "HUN-2D"],
["Aréna", "20230419", "15:20", "HUN-2D"],
["Campona", "20230419", "15:30", "HUN-2D"],
["Aréna", "20230419", "16:40", "HUN-2D"],
["Westend", "20230419", "16:45", "HUN-2D"],
["Campona", "20230419", "17:15", "HUN-2D"],
["Allee", "20230419", "17:30", "HUN-2D"],
["Westend", "20230419", "18:15", "HUN-2D"],
["Campona", "20230419", "18:45", "HUN-2D"],
["Mammut I-II.", "20230419", "18:45", "HUN-2D"],
["Allee", "20230419", "19:00", "HUN-2D"],
["Aréna", "20230419", "19:00", "HUN-2D"],
["Campona", "20230419", "19:45", "HUN-2D"],
["Duna Pláza", "20230419", "20:15", "HUN-2D"],
["Westend", "20230419", "20:15", "HUN-2D"],
["Aréna", "20230419", "20:15", "HUN-2D"],
["Allee", "20230419", "20:45", "HUN-2D"],
["Mammut I-II.", "20230419", "21:00", "HUN-4DX"],
["Mammut I-II.", "20230419", "21:00", "HUN-2D"],
["Aréna", "20230419", "21:40", "HUN-2D"],
["Etele", "20230415", "11:30", "HUN-2D"],
["Etele", "20230412", "16:30", "HUN-2D"],
["Etele", "20230414", "18:30", "HUN-2D"],
["Etele", "20230414", "15:00", "HUN-2D"],
["Etele", "20230415", "10:45", "HUN-2D"],
["Etele", "20230414", "17:30", "HUN-2D"],
["Etele", "20230414", "14:15", "HUN-2D"],
["Etele", "20230413", "14:15", "HUN-2D"],
["Etele", "20230413", "14:00", "HUN-2D"],
["Etele", "20230419", "14:15", "HUN-2D"],
["Etele", "20230418", "17:30", "HUN-2D"],
["Etele", "20230418", "14:15", "HUN-2D"],
["Etele", "20230417", "17:30", "HUN-2D"],
["Etele", "20230417", "14:15", "HUN-2D"],
["Etele", "20230416", "17:30", "HUN-2D"],
["Etele", "20230416", "14:15", "HUN-2D"],
["Etele", "20230416", "10:45", "HUN-2D"],
["Etele", "20230415", "17:30", "HUN-2D"],
["Etele", "20230415", "14:15", "HUN-2D"],
["Etele", "20230419", "15:00", "HUN-2D"],
["Etele", "20230418", "18:30", "HUN-2D"],
["Etele", "20230418", "15:00", "HUN-2D"],
["Etele", "20230417", "18:30", "HUN-2D"],
["Etele", "20230417", "15:00", "HUN-2D"],
["Etele", "20230416", "18:30", "HUN-2D"],
["Etele", "20230416", "15:00", "HUN-2D"],
["Etele", "20230416", "11:30", "HUN-2D"],
["Etele", "20230415", "18:30", "HUN-2D"],
["Etele", "20230415", "15:00", "HUN-2D"],
["Etele", "20230412", "13:15", "HUN-2D"],
["Etele", "20230412", "17:30", "HUN-2D"],
["Etele", "20230412", "14:00", "HUN-2D"],
["Etele", "20230412", "19:45", "HUN-2D"],
];
