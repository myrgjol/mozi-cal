const Calendar = tui.Calendar;

function formatTime(time) {
  const hours = `${time.getHours()}`.padStart(2, '0');
  const minutes = `${time.getMinutes()}`.padStart(2, '0');

  return `${hours}:${minutes}`;
}

Date.prototype.addHours = function(h) {
  this.setTime(this.getTime() + (h*60*60*1000));
  return this;
}

function removeItem(arr, value) { 
  const index = arr.indexOf(value);
  if (index > -1) {
    arr.splice(index, 1);
  }
  return arr;
}

var calendars =
  {
    "Aréna" : {
      id: 'arena',
      name: 'Aréna',
      backgroundColor: 'red',
    },
    'Allee': {
      id: 'allee',
      name: 'Allee',
      backgroundColor: 'yellow',
    },
    'Mammut I-II.': {
      id: 'mammut',
      name: 'Mammut I-II.',
      backgroundColor: 'green',
    },
    "Westend": {
      id: 'westend',
      name: 'Westend',
      backgroundColor: 'blue',
    },
    "Duna Pláza": {
      id: 'dunaplaza',
      name: 'Duna Pláza',
      backgroundColor: 'brown',
    },
    "Campona": {
      id: 'campona',
      name: 'Campona',
      backgroundColor: 'orange',
    },
    "Sugar": {
      id: 'sugar',
      name: 'Sugar',
      backgroundColor: 'purple',
    },
    "Corvin": {
      id: 'corvin',
      name: 'Corvin',
      backgroundColor: 'pink',
    },
    "Etele": {
      id: 'etele',
      name: 'etele',
      backgroundColor: 'brown',
    }
  };

var calendarsForTUI = Object.values(calendars);

var myCalendarString = window.localStorage.getItem("lastUsedCalendars") || "[\"sugar\"]";
var myCalendar = JSON.parse(myCalendarString);
window.localStorage.setItem("lastUsedCalendars", JSON.stringify(myCalendar))

var defaultView = 'week';

if (window.outerWidth < 768) {
  defaultView = 'day';
}

const calendar = new Calendar('#main', {
  defaultView: defaultView,
  template: {
    time(event) {
      const { start, end, title } = event;

      return `<span">${formatTime(start)}~${formatTime(end)} ${title}</span>`;
    }
  },
  calendars: calendarsForTUI,
  isReadOnly: true,
  week: {
      taskView: false,
      eventView: ['time'],
      startDayOfWeek: 1
  },
  day: {
    taskView: false,
    eventView: ['time']
  }
});


document.getElementById("next").addEventListener("click", function (ev) {
  calendar.next();
}, false);

document.getElementById("prev").addEventListener("click", function (ev) {
  calendar.prev();
}, false);

document.getElementById("setting").addEventListener("click", function (ev) {
  document.getElementById("settings").showModal()
}, false);

document.getElementById("closeSettings").addEventListener("click", function (ev) {
  document.getElementById("settings").close()
}, false);

  

events.forEach((ev) => {
  const cinema = calendars[ev[0]].id;
  const dt = ev[1].replace(/(\d{4})(\d{2})(\d{2})/, "$1-$2-$3");
  const tm = ev[2];
  const attributes = ev[3];
  const dateObj = new Date(dt + "T"+tm);
  calendar.createEvents([
    {
      id: cinema + "_" + dt + "_" + tm,
      calendarId: cinema,
      title: cinema + "-" + attributes,
      start: dateObj.toISOString(),
      end: dateObj.addHours(3).toISOString()
    }
  ]);
});

var ind = 0;

document.querySelectorAll("#calendarlist input").forEach(elem => {
  var row = elem.parentElement;
  var savedInd = ind;
  if (myCalendar.find(x => x === elem.id)) {
    elem.checked = true;
  }
  const state = elem.checked;
  calendar.setCalendarVisibility(calendarsForTUI[savedInd].id, state);
  elem.addEventListener("change", (ev) => {
    const state = elem.checked;
    var selectedCalendars = JSON.parse(window.localStorage.getItem("lastUsedCalendars"));
    if (state) {
      selectedCalendars.push(elem.id);
    }
    else {
      removeItem(selectedCalendars, elem.id);
    }
    window.localStorage.setItem("lastUsedCalendars", JSON.stringify(selectedCalendars));
    calendar.setCalendarVisibility(calendarsForTUI[savedInd].id, state);
  }, false);
  row.querySelector("label").addEventListener("click", function (ev) {
    const state = this.previousSibling.checked;
    var selectedCalendars = JSON.parse(window.localStorage.getItem("lastUsedCalendars"));
    if (state) {
      selectedCalendars.push(elem.id);
    }
    else {
      removeItem(selectedCalendars, elem.id);
    }
    window.localStorage.setItem("lastUsedCalendars", JSON.stringify(selectedCalendars));
    calendar.setCalendarVisibility(calendarsForTUI[savedInd].id, state);
    this.previousElementSibling.click();
  }, false);
  ++ind;
})

document.getElementById("viewSwitch").addEventListener("click", function (ev) {
  if (defaultView === 'day') {
    calendar.changeView('week');
    defaultView = 'week';
  }
  else {
    calendar.changeView('day');
    defaultView = 'day';
  }
}, false);